﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

[ApiController]
[Route("[controller]/[action]")]
public class TestController : ControllerBase
{
    private readonly ILogger<TestController> _logger;

    public TestController(ILogger<TestController> logger)
    {
        _logger = logger;
    }

    public string DemoLogInformation()
    {
        _logger.LogInformation("this is the content will be record undercode name log info");
        return "Hello DemoLogInformation!";
    }

    public string DemoLogWarning()
    {
        _logger.LogWarning("this is the content will be record undercode name log warn");
        return "Hello DemoLogWarning!";
    }

    public string DemoLogDebug()
    {
        _logger.LogDebug("this is the content will be record undercode name log debug");
        return "Hello DemoLogDebug!";
    }

    public string DemoException()
    {
        throw new Exception("this is message when exception occur & the content should record by global exception middleware");
    }
}

