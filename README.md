## project 1 goal
1. understand basic logging, log level
2. how to config logging with console
3. how to config log the global exception
## project 2 goal
1. understand the serilog & configuration
2. setup serilog by your self
3. docs: https://github.com/serilog/serilog-aspnetcore

## you had to do
1. record the time exec every request using middleware(time after - time before -> record it with http url, body..etc)
2. Log analysis, Log rotation, Log aggregation, Log retention policy