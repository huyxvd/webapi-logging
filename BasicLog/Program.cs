var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.AddConsole();
});

builder.Services.AddControllers();

var app = builder.Build();

app.UseMiddleware<MyGlobalException>();

app.MapControllers();

app.Run();
