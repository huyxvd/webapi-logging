using Serilog;

var builder = WebApplication.CreateBuilder(args);

// config the seri log
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File(
                path: "logs/log.txt",
                fileSizeLimitBytes: 5 * 1024 * 1024, // 5 MB
                rollingInterval: RollingInterval.Minute) // file will create new every Minutes
    .CreateLogger();

// Add services to the container.
builder.Host.UseSerilog();

builder.Services.AddControllers();

var app = builder.Build();

//app.UseSerilogRequestLogging();

app.UseMiddleware<MyGlobalException>();

app.MapControllers();

app.Run();
